import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

df = pd.read_csv('./giro2_merged.csv', header=0, index_col=False)
df.columns = df.columns.str.replace(' ', '')
df = df.set_index('TIMESTAMP1')
print(df.head())


#df.drop(df[df['PM10'] > 70].index, inplace=True)   # WAS 70 for the first file
df.drop(df[df['PM10'] > 80].index, inplace=True)   # WAS 70 for the first file

#ax = plt.figure()
df['PM10'].plot()
#plt.show()
#import sys; sys.exit(0)

df['PM10_sma5'] = df['PM10'].rolling(window = 5,center = True).mean()
df['PM2_5'].plot()
df['PM1'].plot()
df['PM10_sma5'].plot()
#df.to_csv("giro2_merged_filt_.csv")

#df['PM10_sma10'].plot()
#df['PM10_sma15'].plot()
#plt.show()
#df['PM10'].diff().hist(bins=30)

#df[['PM10', 'PM2_5','PM1']].hist(bins = 30)

#df.plot.scatter(x='PM10', y='PM2_5')
#df.plot.scatter(x='PM10', y='PM1')
#df.plot.scatter(x='PM1', y='PM2_5')

print((df['PM10'] > 56).sum())
print(df[df['PM10'] > 56])
print(df.columns)
print(df['LATITUD_E'])
#df.plot(x='latitude', y='longitude')
print(df[['latitude','longitude']])

#pd.plotting.bootstrap_plot(df['PM10'], size=50, samples=500, color='grey')


plt.show()