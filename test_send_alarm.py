import requests
import json
import jsonschema

car_url = "http://192.168.130.209:5000/alarm"
json_val_url = "http://apic.casaccia.enea.it/schema/mycipcastSchema.json"


def validateJson(json_msg):
    print("Validating JSON")
    schema_file = open(json_val_url, 'r')
    try:
        jsonschema.validate(instance=json_msg, schema=requests.get(json_val_url).json())
    except jsonschema.exceptions.ValidationError as err:
        print(err)
        return False
    print("JSON is valid")
    return True

def main():

    #file = open('./schemas/mycipcast.json', 'r')
    #schema_str = file.read()
    #file.close()
    #alarm_schema = json.loads(schema_str)

    cipcast_msg = {
        "vehicleAlarmId": 1,
        "dateAlarm": "04-05-2020 12:32:47",
        "earthquake":
            {
                "magnitude": 4.5,
                "xlon": 12.7674,
                "ylat": 46.1174,
                "affectedArea": {
                    "points": [
                        {
                            "lat": 12.5,
                            "lon": 45.4
                        },
                        {
                            "lat": 12.5,
                            "lon": 45.4
                        },
                        {
                            "lat": 12.5,
                            "lon": 45.4
                        }
                    ]
                }
            },
        "flood":
            {
                "severity": 22.5,
                "unit": "mm/h",
                "affectedArea": {
                    "points": [
                        {
                            "lat": 12.5,
                            "lon": 45.4
                        },
                        {
                            "lat": 12.5,
                            "lon": 45.4
                        },
                        {
                            "lat": 12.5,
                            "lon": 45.4
                        }
                    ]
                }
            },
        "trafficJams":
            {
                "street": "Via Torrevecchia, 301",
                "affectedStreet": {
                    "points": [
                        {
                            "lat": 12.5,
                            "lon": 45.4
                        },
                        {
                            "lat": 12.5,
                            "lon": 45.4
                        },
                        {
                            "lat": 12.5,
                            "lon": 45.4
                        }
                    ]
                }
            },
        "closedStreets":
            {
                "street": "Via Trionfale 1101",
                "affectedStreet": {
                    "points": [
                        {
                            "lat": 12.5,
                            "lon": 45.4
                        },
                        {
                            "lat": 12.5,
                            "lon": 45.4
                        },
                        {
                            "lat": 12.5,
                            "lon": 45.4
                        }
                    ]
                }
            },

        "landSlide": {
            "street": "Via Trionfale",
            "affectedStreet": {
                "points": [
                    {
                        "lat": 12.5,
                        "lon": 45.4
                    },
                    {
                        "lat": 12.5,
                        "lon": 45.4
                    },
                    {
                        "lat": 12.5,
                        "lon": 45.4
                    }
                ]
            }
        },
        "stoppedVehicle":
            {
                "street": "Via Trionfale",
                "lat": 12.5,
                "lon": 13.4
            },
        "chargeStation":
            {
                "address": "Via Millesimo 18",
                "lat": 11.4,
                "lon": 34.2
            },
        "systemMessage": {
            "content": "Sei disponibile per il Vehicle to Grid?"
        }
    }



    #if(validateJson(msg)):
    headers = { 'Accept': 'application/json', 'Content-Type': 'application/json'}
    print(json.dumps(cipcast_msg ,  indent=4))
    r = requests.post(url=car_url, headers=headers, data=json.dumps(cipcast_msg ))
    print("The pastebin URL is:%s" % r.text)
    print("HTTP status: {} {}", r.status_code, r.reason)


if __name__ == "__main__":
    main()