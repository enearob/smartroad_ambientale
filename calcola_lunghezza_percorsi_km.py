# Nota: usare un virtual environment con geopandas.
# Ad esempio lstm

import geopandas as gpd
import pandas as pd
import  matplotlib.pyplot as plt
import contextily as ctx
import math


filename= "./dati_traiettorie_long_paper/2021-07-09 08_35_07-a.csv"  # 1287.9845850710012
filename= "./dati_traiettorie_long_paper/2021-07-09 08_35_07-b.csv"  # 1363.2844240358993
#filename= "./dati_traiettorie_long_paper/2021-07-09 08_44_59-a.csv"  # 3140.00977880179
#filename= "./dati_traiettorie_long_paper/2021-07-09 08_44_59-b.csv"  # 1787.999344825433
#filename= "./dati_traiettorie_long_paper/2021-07-09 08_55_33-a.csv"  # 1363.443602425426
#filename= "./dati_traiettorie_long_paper/2021-07-09 08_55_33-b.csv"  # 1545.6084148772381
#filename= "./dati_traiettorie_long_paper/2021-07-09 09_06_24-a.csv"  # 580.5446468921533
#filename= "./dati_traiettorie_long_paper/2021-07-09 09_06_24-b.csv"  # 648.4542304772706
#filename= "./dati_traiettorie_long_paper/2021-07-09 09_35_25-a.csv"  # 1003.2162022922753
#filename= "./dati_traiettorie_long_paper/2021-07-09 09_35_25-b_corretto_.csv"  # 1413.026287316356
df = pd.read_csv(filename, skiprows=3, sep = ';', skipinitialspace=True)
df.rename(columns={df.columns[0]: df.columns[0].replace('# ', '')}, inplace=True)


"""
df= df[['TIMESTAMP1', 'TIMESTAMP2', 'GPS_STATUS', 'LATITUDE', 'LONGITUDE',
       'ALTITUDE', 'ROLL', 'PITCH', 'YAW', 'TEMPERATURE_P ', 'CO', 'NO',
       'NO2 ', 'O3', 'SO2', 'VOC', 'HUMIDITY', 'PRESSURE', 'TEMP_H',
       'TEMP_EXT', 'PM1', 'PM2_5', 'PM10', 'Bin0', 'Bin1', 'Bin2', 'Bin3',
       'Bin4', 'Bin5', 'Bin6', 'Bin7', 'Bin8', 'Bin9', 'Bin10', 'Bin11',
       'Bin12', 'Bin13', 'Bin14', 'Bin15', 'Bin1MToF', 'Bin3MToF', 'Bin5MToF',
       'Bin7MToF', 'PM_SampleFlowRate', 'PM_SamplePeriod']]
"""

gdf = gpd.GeoDataFrame(df, geometry=gpd.points_from_xy(df['LONGITUDE'], df['LATITUDE']), crs='EPSG:4326')
# math.sqrt((1395245.747-1395247.584 )**2 + ( 5153124.322 - 5153124.262)**2)
#gdf_ = gdf.to_crs(epsg=26592)
gdf_ = gdf.to_crs(epsg=3034)
points  =gdf_.geometry
points2 = points.shift() #We shift the dataframe by 1 to align pnt1 with pnt2
points2.crs = points.crs
print(points.distance(points2))
print(points.distance(points2).sum())


#print(math.sqrt((gdf_.geometry.iloc[0].x - gdf_.geometry.iloc[4].x )**2 + ( gdf_.geometry.iloc[0].y - gdf_.geometry.iloc[4].y)**2))
ax = gdf_.to_crs(epsg=3857).plot()
ctx.add_basemap(ax=ax, zoom=16)
plt.show()

pass

