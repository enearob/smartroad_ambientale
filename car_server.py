import flask
import json

cipcast_server_url = "http://192.168.153.96:6080/cipcaster-restws/rest/putDataToCipcast"
json_val_url = "http://apic.casaccia.enea.it/schema/mycipcastSchema.json"

app = flask.Flask(__name__)
app.config["DEBUG"] = True


@app.route('/alarm', methods=['POST'])
def home():
    print("Msg received")
    if flask.request.json:
        print("JSON received")
        msg = flask.request.json
        print(msg)
    else:
        return "No JSON provided !!"

    return "Msg received"

app.run(host="0.0.0.0")