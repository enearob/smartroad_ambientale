import requests
#import pandas as pd
import json
from datetime import date
from datetime import datetime
import jsonschema
import paramiko
import time
import pprint
import sys
from optparse import OptionParser
import time
import os

parser = OptionParser()
parser.add_option("-a", "--address", dest="address", type="string", help="remote ip address")
parser.add_option("-f", "--log_folder", dest="log_folder", type="string", help="folder log on remote machine")
parser.add_option("-u", "--user", dest="user", type="string", help="remore username")
parser.add_option("-p", "--password", dest="password", type="string", help="remote password")
parser.add_option("-i", "--id", dest="vehicle_id", type="string", help="id of the vehicle", default="1")
parser.add_option("-n", "--name", dest="vehicle_name", type="string", help="name of the vehicle", default="BePos")
parser.add_option("-s", "--server", dest="cipcast_server_url", type="string", help="rest web service url", default="http://marte.dhcpnet.casaccia:8080/cipcaster-restws/rest/putDataToCipcast")
parser.add_option("-v", "--schema", dest="json_val_url", type="string", help="json_validation_schema url", default="http://marte.dhcpnet.casaccia:8080/cipcaster-restws/validate/vehicle")

(options, args) = parser.parse_args()

user = options.user
password = options.password
log_folder = options.log_folder
address = options.address
vehicle_name = options.vehicle_name
vehicle_id = int(options.vehicle_id)
cipcast_server_url = options.cipcast_server_url
json_val_url = options.json_val_url

def get_msg(id_veh, name_veh, date, speed, latitude, longitude, altitude, heading, environment):
    msg = {
        "autonomous": False,
        "type": "car",
        "dateTime": date,
        "speed": speed,
        "lat": latitude,
        "lon": longitude,
        "altitude": altitude,
        "heading": heading,
        "battery": {
            "autonomyKm": 0,
            "autonomyTime": 0,
            "avgConsumption": 0,
            "consumption": 0,
            "batteryLevel": 0
        },
        "vehicleId": {
            "idVehicle": id_veh,
            "name": name_veh
        },

        "environment": environment
        #"route": route,
        #"message": message

    }

    return msg

def validateJson(json_msg):
    print("Validating JSON")
    print("schema url:", json_val_url)
    try:
        jsonschema.validate(instance=json_msg, schema=requests.get(json_val_url).json())
    except jsonschema.exceptions.ValidationError as err:
        print(err)
        return False
    print("JSON is valid")
    return True


def main():

    print("connecting to {} user {} password {}".format(address, user, password))
    ssh_client = paramiko.SSHClient()
    ssh_client.load_system_host_keys()
    ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh_client.connect(address, username=user, password=password)

    init_rows_count = 0
    init_rows = 4

#    log_folder = "/".join(remotefile.split("/")[:-1])
    print("log_folder:",log_folder)
    cmd = "ls {} -atlr | tail -n 1".format(log_folder)
    print("executing command: {}".format(cmd))
    stdin, stdout, stderr = ssh_client.exec_command(cmd)
    out = " ".join(stdout.read().decode().strip().split(" ")[-2:])
    print(out)
    remotefile = os.path.join(log_folder,out)
    remotefile = remotefile.replace(" ","\ ")
    print("remote file", remotefile)

    while init_rows_count <= init_rows:
        cmd = "wc -l {}".format(remotefile)
        stdin, stdout, stderr = ssh_client.exec_command(cmd)
        print("executing command: {}".format(cmd))
        out = stdout.read().decode().strip().split(" ")[0]
        print(out, "\n")
        init_rows_count = int(out)
        print("Initializing {}/{}".format(init_rows_count, init_rows ))

    print("Inizio acquisizione e invio.")        
    
    time_prec = ""

    while True:

        cmd = "tail -n 1 {}".format(remotefile)
        print("Executing command ", cmd)
        stdin, stdout, stderr = ssh_client.exec_command(cmd)
        out = stdout.read().decode().strip()
        out = out.split(",")
        print(out)

        timestamp=int(float(out[1]))
        co=float(out[10])
        no=float(out[11])
        no2=float(out[12])    
        o3=float(out[13])
        so2=float(out[14])
        voc=float(out[15])
        pm1=float(out[20])
        pm2_5=float(out[21])
        pm10=float(out[22])
        lat=float(out[3])
#        lat=12
#        lon=44
        lon=float(out[4])
        alt=int(float(out[5]))
#        alt=100
        heading=int(float(out[8]))
#        heading=7


        environment = {
                "dateTimeEnv": datetime.fromtimestamp(timestamp).strftime("%Y-%m-%d %H:%M:%S"),
                "envData": [
                    {
                        "name": "CO",
                        "val": co,
                        "unit": "ppm"
                    },
                    {
                        "name": "NO",
                        "val": no,
                        "unit": "ppm"
                    },
                    {
                        "name": "O3",
                        "val": o3,
                        "unit": "ppm"
                    },
                    {
                        "name": "SO2",
                        "val": so2,
                        "unit": "ppm"
                    },
                    {
                        "name": "VOC",
                        "val": voc,
                        "unit": "Ohm"
                    },
                    {
                        "name": "PM1",
                        "val": pm1,
                        "unit": "ppm"
                    },
                    {
                        "name": "PM2_5",
                        "val": pm2_5,
                        "unit": "ppm"
                    },
                    {
                        "name": "PM10",
                        "val": pm10,
                        "unit": "ppm"
                    },
                ]
        }
        print("vechicle_name",vehicle_name)
        print("vechicle_id",vehicle_id)
        msg = get_msg(id_veh=vehicle_id,
                      name_veh=vehicle_name,
                      date = str(datetime.now().strftime("%m-%d-%Y %H:%M:%S")),
                      speed = 0,
                      latitude= lat,
                      longitude=lon,
                      altitude =alt,
                      heading=heading,
                      environment=environment,
                      )

        if(validateJson(msg)):
            headers = { 'Accept': 'application/json', 'Content-Type': 'application/json'}
            print(json.dumps(msg,  indent=4))
            r = requests.post(url=cipcast_server_url, headers=headers, data=json.dumps(msg))
            print("The pastebin URL is:%s" % r.text)
            print("HTTP status: {} {}", r.status_code, r.reason)

        time.sleep(2)
    ssh_client.close()

if __name__ == "__main__":
    main()
