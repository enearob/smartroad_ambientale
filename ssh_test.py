import paramiko
import sys
from optparse import OptionParser


parser = OptionParser()
parser.add_option("-a", "--address", dest="address", type="string", help="remote ip address")
parser.add_option("-f", "--file", dest="filename", type="string", help="file path on remote machine")
parser.add_option("-u", "--user", dest="user", type="string", help="remore username")
parser.add_option("-p", "--password", dest="password", type="string", help="remote password")

(options, args) = parser.parse_args()

user = options.user
password = options.password
remotefile = options.filename
address = options.address

print("connecting to {} user {} password {}".format(address, user, password))


ssh_client = paramiko.SSHClient()
ssh_client.load_system_host_keys()
ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh_client.connect(address, username=user, password=password)

cmd = "tail -n 1 {}".format(remotefile)
#cmd = "ls"
print("Executing command ", cmd)
stdin, stdout, stderr = ssh_client.exec_command(cmd)
#print("stdout:",stdout)
#print("stderr:", stderr)
#print("stdin:", stdin)

out = stdout.read().decode().strip()
out = out.split(",")
print(out)


#error = stderr.read().decode().strip()

#if self.log_level:
#    logger.info(out)
#if error:
#    raise Exception('There was an error pulling the runtime: {}'.format(error))
ssh_client.close()
